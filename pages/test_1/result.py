from selenium.webdriver.common.by import By
class YandexResultPage:
    SEARCH_INPUT = (By.CLASS_NAME, 'path__item')

    def __init__(self, browser):
        self.browser = browser
    def link_div_count(self):
        link_divs = self.browser.find_elements(*self.SEARCH_INPUT)
        return len(link_divs)
    def search_count(self, link):
        link_divs = self.browser.find_elements(*self.SEARCH_INPUT)
        for i in range(5):
            if link_divs[i].get_attribute('href').startswith(link):
                return 1
        return 0
