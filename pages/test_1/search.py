from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
class YandexSearchPage:
    URL = 'https://www.yandex.ru'
    SEARCH_INPUT = (By.ID, 'text')

    def __init__(self, browser):
        self.browser = browser
    def load(self):
        self.browser.get(self.URL)
    def exist(self):
        search_input = self.browser.find_elements_by_xpath(".//*[@id='text']")
        return len(search_input)
    def search(self, phrase):
        search_input = self.browser.find_element(*self.SEARCH_INPUT)
        search_input.send_keys(phrase)
        suggest = self.browser.find_elements_by_class_name('mini-suggest__popup-content')
        search_input.send_keys(Keys.RETURN)
        return len(suggest)
