from selenium.webdriver.common.by import By
class YandexImagePage:
    URL = 'https://yandex.ru/images/'
    CLASS = 'PopularRequestList-Item_pos_0'
    FIRST_CATEGORY = (By.CLASS_NAME, CLASS)
    FIRST_CATEGORY_LINK = (By.CSS_SELECTOR, 'a.PopularRequestList-Preview')

    def __init__(self, browser):
        self.browser = browser
    def url_check(self):
        current_url = self.browser.current_url
        if current_url.startswith(self.URL):
            return 1
        return 0
    def first_category_press(self):
        first_category_image = self.browser.find_element(*self.FIRST_CATEGORY)
        NAME_OF_CATEGORY = first_category_image.get_attribute('data-grid-text')
        first_category_image_link = self.browser.find_element(*self.FIRST_CATEGORY_LINK)
        self.browser.get(first_category_image_link.get_attribute('href'))
        return NAME_OF_CATEGORY
