from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
class YandexImageResult:
    SEARCH_INPUT = (By.CLASS_NAME, 'input__control')
    FIRST_IMAGE_CLICK = (By.CLASS_NAME, 'serp-item_pos_0')
    OPENED_IMAGE_CHECK = (By.CLASS_NAME, 'MMSidebar_view_block')
    IMAGE_SRC = (By.CLASS_NAME, 'MMImage-Origin')

    def __init__(self, browser):
        self.browser = browser
    def search_input_value(self):
        search_input = self.browser.find_element(*self.SEARCH_INPUT)
        first_image = self.browser.find_element(*self.FIRST_IMAGE_CLICK)
        first_image.click()
        time.sleep(1)
        first_image_src = self.browser.find_element(*self.IMAGE_SRC).get_attribute('src')
        return search_input.get_attribute('value'), first_image_src
    def first_image_press(self):
        try:
            first_image_opened = self.browser.find_element(*self.OPENED_IMAGE_CHECK)
            return 1
        except NoSuchElementException:
            return 0
    def right_press(self):
        next_image = self.browser.find_element_by_css_selector('body').send_keys(Keys.RIGHT)
        time.sleep(1)
        return self.browser.find_element(*self.IMAGE_SRC).get_attribute('src')
    def left_press(self):
        next_image = self.browser.find_element_by_css_selector('body').send_keys(Keys.LEFT)
        time.sleep(1)
        return self.browser.find_element(*self.IMAGE_SRC).get_attribute('src')
