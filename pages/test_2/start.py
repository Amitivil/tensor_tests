from selenium.webdriver.common.by import By
class YandexStartPage:
    URL = 'https://www.yandex.ru'
    IMAGE = (By.XPATH, "//a[@data-id='images']")

    def __init__(self, browser):
        self.browser = browser
    def load(self):
        self.browser.get(self.URL)
    def check_image_button(self):
        try:
            image_button = self.browser.find_element(*self.IMAGE)
            return 1
        except:
            return 0
    def press_image_button(self):
        image_button = self.browser.find_element(*self.IMAGE)
        self.browser.get(image_button.get_attribute('href'))
