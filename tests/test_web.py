import pytest

from pages.test_1.result import YandexResultPage
from pages.test_1.search import YandexSearchPage

def test_basic_yandex_search(browser):
    PHRASE = 'Тензор'
    LINK = 'https://tensor.ru'
    search_page = YandexSearchPage(browser)
    search_page.load()
    assert search_page.exist() > 0, 'No search field'
    assert search_page.search(PHRASE) > 0, 'No suggest field'

    result_page = YandexResultPage(browser)
    assert result_page.link_div_count() > 0, 'No search results'
    assert result_page.search_count(LINK) > 0, 'No link in the results from first to fifth'
