import pytest

from pages.test_2.start import YandexStartPage
from pages.test_2.image import YandexImagePage
from pages.test_2.image_search_result import YandexImageResult

def test_image_yandex_check(browser):
    start_page = YandexStartPage(browser)
    start_page.load()
    assert start_page.check_image_button() > 0, 'No "Картинки" link'
    start_page.press_image_button()

    image_page = YandexImagePage(browser)
    assert image_page.url_check() > 0, 'URL is not "https://yandex.ru/images/"'
    search_input = image_page.first_category_press()

    category_page = YandexImageResult(browser)
    search_input_infact, first_image_src = category_page.search_input_value()
    assert search_input_infact == search_input, 'Text of first category is wrong'
    assert category_page.first_image_press() > 0, 'First image was not opened'
    assert category_page.right_press() != first_image_src, 'Right click does not lead to the next image'
    assert category_page.left_press() == first_image_src, 'Left click does not return to the previous image'
